#include <iostream>
using namespace std;

int main() {
    string nama;
    double gajiPokok, tunjangan, potongan, gajiBersih;

    // Input data karyawan
    cout << "Masukkan nama karyawan: ";
    getline(cin, nama);
    cout << "Masukkan gaji pokok: ";
    cin >> gajiPokok;
    cout << "Masukkan tunjangan: ";
    cin >> tunjangan;
    cout << "Masukkan potongan: ";
    cin >> potongan;

    // Hitung gaji bersih
    gajiBersih = gajiPokok + tunjangan - potongan;

    // Tampilkan hasil
    cout << "===== Rincian Gaji Karyawan =====" << endl;
    cout << "Nama Karyawan: " << nama << endl;
    cout << "Gaji Pokok: " << gajiPokok << endl;
    cout << "Tunjangan: " << tunjangan << endl;
    cout << "Potongan: " << potongan << endl;
    cout << "Gaji Bersih: " << gajiBersih << endl;

    return 0;
}
